﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CodingAssignment.Models;
using CodingAssignment.Services.Interfaces;
using Newtonsoft.Json;

namespace CodingAssignment.Services
{
    public class FileManagerService: IFileManagerService
    {
        public DataFileModel GetData()
        {
            var data = JsonConvert.DeserializeObject<DataFileModel>(File.ReadAllText("./AppData/DataFile.json"));

            return data;
        }

        public DataModel GetDataById(int id)
        {
            var data = JsonConvert.DeserializeObject<DataFileModel>(File.ReadAllText("./AppData/DataFile.json"));
            var val = data.Data.FirstOrDefault(t => t.Id == id);

            return val;
        }

        public bool Insert(DataModel model)
        {
            var data = JsonConvert.DeserializeObject<DataFileModel>(File.ReadAllText("./AppData/DataFile.json"));
            int dataCount = data.Data.Count;
            data.Data.Add(model);
            int newDataCount = data.Data.Count;
            var convertedJson = JsonConvert.SerializeObject(data, Formatting.Indented);

            File.WriteAllText("./AppData/DataFile.json", convertedJson);

            if (newDataCount > dataCount)
            {
                return true;
            }

            return false;
        }

        public bool Update(DataModel model, int id)
        {
            var data = JsonConvert.DeserializeObject<DataFileModel>(File.ReadAllText("./AppData/DataFile.json"));
            data.Data.Where(x => x.Id == id).ToList().ForEach(y => y.DataString = model.DataString);
            var convertedJson = JsonConvert.SerializeObject(data, Formatting.Indented);
            File.WriteAllText("./AppData/DataFile.json", convertedJson);

            return true;
        }

        public bool Delete(int id)
        {
            var data = JsonConvert.DeserializeObject<DataFileModel>(File.ReadAllText("./AppData/DataFile.json"));

            for (int i = 0; i < data.Data.Count; i++)
            {
                if (data.Data[i].Id.Equals(id))
                {
                    data.Data.RemoveAt(i);
                }
            }

            var convertedJson = JsonConvert.SerializeObject(data, Formatting.Indented);
            File.WriteAllText("./AppData/DataFile.json", convertedJson);

            return true;
        }
    }
}
