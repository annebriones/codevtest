﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodingAssignment.Models
{
    public class DataModel
    {
        public int Id { get; set; }

        public List<string> DataString { get; set; }

    }
}