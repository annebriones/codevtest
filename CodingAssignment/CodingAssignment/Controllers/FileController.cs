﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodingAssignment.Models;
using CodingAssignment.Services;
using CodingAssignment.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CodingAssignment.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {

        private IFileManagerService _fileManger;

        public FileController(IFileManagerService _fileManger)
        {
            this._fileManger = _fileManger;
        }

        [HttpGet]
        public DataFileModel Get()
        {
            var data = _fileManger.GetData();
            return data;
        }

        [HttpGet("{id}")]
        public DataModel Get(int id)
        {
            var data = _fileManger.GetDataById(id);
            return data;
        }

        [HttpPost]
        public DataFileModel Post([FromBody]DataModel model)
        {
            DataFileModel dataFile = new DataFileModel();
            List<DataModel> dataList = new List<DataModel>();
            dataList.Add(model);

            _fileManger.Insert(model);
            dataFile.Data = dataList;

            return dataFile;
        }

        [HttpPut]
        public DataFileModel Put(DataModel model, int id)
        {
            DataFileModel dataFile = new DataFileModel();
            List<DataModel> dataList = new List<DataModel>();
            dataList.Add(model);

            _fileManger.Update(model, id);

            return dataFile;
        }

        [HttpDelete]
        public DataFileModel Delete(int id)
        {
            DataFileModel dataFile = new DataFileModel();

            _fileManger.Delete(id);

            return dataFile;
        }

        
    }
}
